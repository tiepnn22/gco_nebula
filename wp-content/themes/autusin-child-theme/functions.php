<?php add_action( 'wp_enqueue_scripts', 'ya_enqueue_styles' );
function ya_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}



// Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
// Disables the block editor from managing widgets.
add_filter( 'use_widgets_block_editor', '__return_false' );
// Use Block Editor default for Post
add_filter('use_block_editor_for_post', '__return_false');

// Close all Update
if (!function_exists('core_remove_core_updates')) {
    function core_remove_core_updates() {
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','core_remove_core_updates');
    add_filter('pre_site_transient_update_plugins','core_remove_core_updates');
    add_filter('pre_site_transient_update_themes','core_remove_core_updates');
}


// Remove Version Css Js
function core_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
if (!is_admin()) add_filter( 'style_loader_src', 'core_remove_ver_css_js', 9999 );
if (!is_admin()) add_filter( 'script_loader_src', 'core_remove_ver_css_js', 9999 );




add_action( 'wp_footer', 'my_footer_scripts' );
function my_footer_scripts(){
?>
    <script type="text/javascript">
        // var scroll = jQuery('div').width();
        // if(scroll >= 992) {
            var height = jQuery('.nav.nav-pills.nav-mega .autusin-mega-menu .dropdown-menu .dropdown-sub');
            var height2 = height.height();
            console.log(height2);
            
            var jQuerycontent = jQuery('.nav.nav-pills.nav-mega .autusin-mega-menu .dropdown-menu');
            jQuerycontent.height(height2);
        // }


        // jQuery('.nav.nav-pills.nav-mega .autusin-mega-menu .dropdown-menu > li').hover(function(){
        //     jQuery(this).find('> ul').css('visibility', 'visible');
        // },function(e) {
        //     jQuery(this).find('> ul').css('visibility', 'hidden');
        // });
    </script>
<?php
}